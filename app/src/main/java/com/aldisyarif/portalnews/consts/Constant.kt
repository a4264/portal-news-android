package com.aldisyarif.portalnews.consts

object Constant {
    const val DATA_NEWS = "DATA_NEWS"
    const val DATA_BOOKMARK = "DATA_BOOKMARK"
    const val CATEGORY = "CATEGORY"
    const val SOURCE = "SOURCE"
}