package com.aldisyarif.portalnews.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

data class ServiceResponse<T>(
    val totalResults: Int?,
    val articles: List<T>?,
    val status: String?,
    val code: String?,
    val message: String?,
)

data class SourcesResponse<T>(
    val sources: List<T>?,
    val status: String?
)

@Parcelize
data class Source(
    val name: String?,
    val id: String?
): Parcelable

@Parcelize
data class ArticlesItem(
    val publishedAt: String?,
    val author: String?,
    val urlToImage: String?,
    val description: String?,
    val source: Source?,
    val title: String?,
    val url: String?,
    val content: String?
): Parcelable

@Parcelize
data class SourcesItem(
    val country: String?,
    val name: String?,
    val description: String?,
    val language: String?,
    val id: String?,
    val category: String?,
    val url: String?
): Parcelable
