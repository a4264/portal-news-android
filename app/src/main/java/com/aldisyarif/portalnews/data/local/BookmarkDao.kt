package com.aldisyarif.portalnews.data.local

import androidx.room.*
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface BookmarkDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun bookmarkNews(bookmarkEntity: BookmarkEntity): Long

    @Delete
    suspend fun unBookmarkNews(bookmarkEntity: BookmarkEntity): Int

    @Query("SELECT * FROM tb_bookmark_entities WHERE title = :title")
    fun getDetailBookmarkNews(title: String): Flow<BookmarkEntity>

    @Query("SELECT * FROM tb_bookmark_entities")
    fun listBookmarkNews(): Flow<List<BookmarkEntity>>
}