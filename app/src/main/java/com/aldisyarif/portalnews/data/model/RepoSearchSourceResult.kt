package com.aldisyarif.portalnews.data.model

import java.lang.Exception

sealed class RepoSearchSourceResult {
    data class Success(val data: List<SourcesItem>) : RepoSearchSourceResult()
    data class Error(val error: Exception) : RepoSearchSourceResult()
}
