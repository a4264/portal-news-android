package com.aldisyarif.portalnews.data.model

import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.aldisyarif.portalnews.R

data class CategoryItem(
    @param:DrawableRes
    val icon: Int,
    @StringRes
    val title: Int
)
