package com.aldisyarif.portalnews.data.model

sealed class UiAction {
    data class Search(val query: String) : UiAction()
    data class Scroll(val currentQuery: String) : UiAction()
}

data class UiState(
    val query: String = "",
    val lastQueryScrolled: String = "",
    val hasNotScrolledForCurrentSearch: Boolean = false
)

sealed class UiSourceModel {
    data class RepoItem(val source: SourcesItem) : UiSourceModel()
    data class SeparatorItem(val description: String) : UiSourceModel()
}

sealed class UiArticleModel {
    data class RepoItem(val article: ArticlesItem) : UiArticleModel()
}