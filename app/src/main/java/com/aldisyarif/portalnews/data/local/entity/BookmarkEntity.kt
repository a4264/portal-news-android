package com.aldisyarif.portalnews.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aldisyarif.portalnews.data.model.Source
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "tb_bookmark_entities")
data class BookmarkEntity(
    @PrimaryKey(autoGenerate = true)
    val dataId: Int = 0,

    @ColumnInfo(name = "published_at")
    val publishedAt: String?,

    @ColumnInfo(name = "author")
    val author: String?,

    @ColumnInfo(name = "url_image")
    val urlToImage: String?,

    @ColumnInfo(name = "description")
    val description: String?,

    @Embedded(prefix = "source_")
    val source: Source?,

    @ColumnInfo(name = "title")
    val title: String?,

    @ColumnInfo(name = "url")
    val url: String?,

    @ColumnInfo(name = "content")
    val content: String?
):Parcelable
