package com.aldisyarif.portalnews.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity

@Database(
    entities = [
        BookmarkEntity::class,
    ],
    version = 1,
    exportSchema = false
)
abstract class BookmarkDatabase: RoomDatabase() {
    abstract fun bookmarkDao(): BookmarkDao
}