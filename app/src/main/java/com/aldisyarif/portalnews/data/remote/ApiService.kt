package com.aldisyarif.portalnews.data.remote

import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.data.model.ServiceResponse
import com.aldisyarif.portalnews.data.model.SourcesItem
import com.aldisyarif.portalnews.data.model.SourcesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("top-headlines")
    suspend fun getTopHeadline(
        @Query("country") country: String?=null,
        @Query("q") query: String?=null,
        @Query("apiKey") apiKey: String,
        @Query("category") category: String? = null,
        @Query("sources") sources: String?=null,
        @Query("pageSize") pageSize: String,
        @Query("page") page: String,
    ): ServiceResponse<ArticlesItem>

    @GET("everything")
    suspend fun getSearchNews(
        @Query("q") q: String,
        @Query("apiKey") apiKey: String,
        @Query("pageSize") pageSize: String,
        @Query("page") page: String,
    ): ServiceResponse<ArticlesItem>

    @GET("top-headlines/sources")
    suspend fun getSources(
        @Query("apiKey") apiKey: String,
        @Query("category") category: String? = null,
        @Query("pageSize") pageSize: String,
        @Query("page") page: String,
    ): SourcesResponse<SourcesItem>
}