package com.aldisyarif.portalnews.enums

enum class RequestStatus {
    SUCCESS,
    LOADING,
    ERROR,
}