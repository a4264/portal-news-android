package com.aldisyarif.portalnews.utils

import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.data.model.CategoryItem

object GenerateData {
    fun getListCategory(): List<CategoryItem>{
        return listOf(
            CategoryItem(
                R.drawable.ic_business,
                R.string.business
            ),
            CategoryItem(
                R.drawable.ic_entertainment,
                R.string.entertainment
            ),
            CategoryItem(
                R.drawable.ic_general,
                R.string.general
            ),
            CategoryItem(
                R.drawable.ic_health,
                R.string.health
            ),
            CategoryItem(
                R.drawable.ic_science,
                R.string.science
            ),
            CategoryItem(
                R.drawable.ic_sport,
                R.string.sports
            ),
            CategoryItem(
                R.drawable.ic_technology,
                R.string.technology
            )
        )
    }
}