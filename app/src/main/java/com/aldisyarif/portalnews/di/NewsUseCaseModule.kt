package com.aldisyarif.portalnews.di

import com.aldisyarif.portalnews.repository.INewsRepository
import com.aldisyarif.portalnews.usecase.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object NewsUseCaseModule {

    @Provides
    fun provideGetTopHeadlineUseCase(
        repository: INewsRepository
    ): GetTopHeadlineUseCase =
        GetTopHeadlineUseCase(repository)

    @Provides
    fun provideGetSourcesUseCase(
        repository: INewsRepository
    ): GetSourcesUseCase =
        GetSourcesUseCase(repository)


    @Provides
    fun provideGetArticlesUseCase(
        repository: INewsRepository
    ): GetArticlesUseCase =
        GetArticlesUseCase(repository)

//    @Provides
//    fun provideGetSearchNewsUseCase(
//        repository: INewsRepository
//    ): GetSearchNewsUseCase =
//        GetSearchNewsUseCase(repository)

    @Provides
    fun provideGetListBookmarkNewsUseCase(
        repository: INewsRepository
    ): GetListBookmarkNewsUseCase =
        GetListBookmarkNewsUseCase(repository)

    @Provides
    fun provideSaveBookmarkNewsUseCase(
        repository: INewsRepository
    ): SaveBookmarkNewsUseCase =
        SaveBookmarkNewsUseCase(repository)

    @Provides
    fun provideGetDetailBookmarkNewsUseCase(
        repository: INewsRepository
    ): GetDetailBookmarkNewsUseCase =
        GetDetailBookmarkNewsUseCase(repository)

    @Provides
    fun provideDeleteUnBookmarkUseCase(
        repository: INewsRepository
    ): DeleteUnBookmarkUseCase =
        DeleteUnBookmarkUseCase(repository)
}