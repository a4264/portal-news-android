package com.aldisyarif.portalnews.di

import android.content.Context
import androidx.room.Room
import com.aldisyarif.portalnews.data.local.BookmarkDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object BookmarkDatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        BookmarkDatabase::class.java,
        "bookmark_db"
    ).build()

    @Singleton
    @Provides
    fun provideBookmark(
        db: BookmarkDatabase
    ) = db.bookmarkDao()
}