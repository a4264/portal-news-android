package com.aldisyarif.portalnews.di

import com.aldisyarif.portalnews.data.local.BookmarkDao
import com.aldisyarif.portalnews.data.remote.ApiService
import com.aldisyarif.portalnews.repository.INewsRepository
import com.aldisyarif.portalnews.repository.NewsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object NewsRepositoryModule {
    @Provides
    fun provideRepository(
        service: ApiService,
        db: BookmarkDao
    ): INewsRepository =
        NewsRepositoryImpl(service, db)
}