package com.aldisyarif.portalnews.repository

import com.aldisyarif.portalnews.data.local.BookmarkDao
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.data.model.ServiceResponse
import com.aldisyarif.portalnews.data.model.SourcesItem
import com.aldisyarif.portalnews.data.model.SourcesResponse
import com.aldisyarif.portalnews.data.remote.ApiService
import kotlinx.coroutines.flow.Flow

class NewsRepositoryImpl(
    private val service: ApiService,
    private val db: BookmarkDao
): INewsRepository {
    override suspend fun getTopHeadline(
        country: String?,
        q: String?,
        apiKey: String,
        category: String?,
        sources: String?,
        pageSize: String,
        page: String
    ): ServiceResponse<ArticlesItem> =
        service.getTopHeadline(country, q, apiKey, category, sources, pageSize, page)

    override suspend fun getSearchNews(
        q: String,
        apiKey: String,
        pageSize: String,
        page: String,
    ): ServiceResponse<ArticlesItem> =
        service.getSearchNews(q, apiKey, pageSize, page)

    override suspend fun getSources(
        apiKey: String,
        category: String?,
        pageSize: String,
        page: String,
    ): SourcesResponse<SourcesItem> =
        service.getSources(apiKey, category, pageSize, page)

    override suspend fun bookmarkNews(bookmarkEntity: BookmarkEntity): Long =
        db.bookmarkNews(bookmarkEntity)

    override suspend fun unBookmarkNews(bookmarkEntity: BookmarkEntity): Int =
        db.unBookmarkNews(bookmarkEntity)

    override suspend fun getDetailBookmarkNews(title: String): Flow<BookmarkEntity> =
        db.getDetailBookmarkNews(title)

    override suspend fun listBookmarkNews(): Flow<List<BookmarkEntity>> =
        db.listBookmarkNews()


}