package com.aldisyarif.portalnews.repository

import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.data.model.ServiceResponse
import com.aldisyarif.portalnews.data.model.SourcesItem
import com.aldisyarif.portalnews.data.model.SourcesResponse
import kotlinx.coroutines.flow.Flow

interface INewsRepository {

    suspend fun getTopHeadline(
        country: String? = null,
        q: String?,
        apiKey: String,
        category: String? = null,
        sources: String? = null,
        pageSize: String,
        page: String,
    ): ServiceResponse<ArticlesItem>

    suspend fun getSearchNews(
        q: String,
        apiKey: String,
        pageSize: String,
        page: String,
    ): ServiceResponse<ArticlesItem>

    suspend fun getSources(
        apiKey: String,
        category: String?,
        pageSize: String,
        page: String,
    ): SourcesResponse<SourcesItem>

    suspend fun bookmarkNews(
        bookmarkEntity: BookmarkEntity
    ): Long

    suspend fun unBookmarkNews(
        bookmarkEntity: BookmarkEntity
    ): Int

    suspend fun getDetailBookmarkNews(
        title: String
    ): Flow<BookmarkEntity>

    suspend fun listBookmarkNews(): Flow<List<BookmarkEntity>>
}