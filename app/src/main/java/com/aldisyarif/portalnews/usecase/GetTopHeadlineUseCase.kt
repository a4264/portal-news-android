package com.aldisyarif.portalnews.usecase

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.filter
import com.aldisyarif.portalnews.base.BaseUseCase
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.paging.NewsPagingSource
import com.aldisyarif.portalnews.repository.INewsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetTopHeadlineUseCase(
    private val repository: INewsRepository
): BaseUseCase() {

    operator fun invoke(
        country: String = "us",
        category: String?,
    ): Flow<PagingData<ArticlesItem>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true
            ),
            pagingSourceFactory = {
                NewsPagingSource(
                    repository,
                    null,
                    country,
                    category,
                    null,
                    apiKey
                )
            }
        ).flow
    }
}