package com.aldisyarif.portalnews.usecase

import com.aldisyarif.portalnews.base.BaseUseCase
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.repository.INewsRepository
import com.aldisyarif.portalnews.utils.Resources
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class GetDetailBookmarkNewsUseCase(
    private val repository: INewsRepository
): BaseUseCase() {

    operator fun invoke(title: String): Flow<Resources<BookmarkEntity>> {
        return flow {
            emit(Resources.loading())
            try {
                val db = repository.getDetailBookmarkNews(title)
                db.collect {
                    emit(Resources.success(it))
                }
            } catch (e: Throwable){
                emit(Resources.error(e.localizedMessage ?: "", null))
            }
        }.flowOn(Dispatchers.IO)
    }

}