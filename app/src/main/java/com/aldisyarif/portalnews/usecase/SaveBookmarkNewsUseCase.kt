package com.aldisyarif.portalnews.usecase

import com.aldisyarif.portalnews.base.BaseUseCase
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.repository.INewsRepository
import com.aldisyarif.portalnews.utils.Resources
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class SaveBookmarkNewsUseCase(
    private val repository: INewsRepository
): BaseUseCase() {

    operator fun invoke(bookmarkEntity: BookmarkEntity): Flow<Resources<Long>> {
        return flow {
            emit(Resources.loading())
            try {
                emit(Resources.success(repository.bookmarkNews(bookmarkEntity)))
            } catch (e: Throwable){
                emit(Resources.error(e.localizedMessage ?: "", null))
            }
        }.flowOn(Dispatchers.IO)
    }
}