package com.aldisyarif.portalnews.usecase

import androidx.paging.*
import com.aldisyarif.portalnews.base.BaseUseCase
import com.aldisyarif.portalnews.data.model.RepoSearchSourceResult
import com.aldisyarif.portalnews.data.model.SourcesItem
import com.aldisyarif.portalnews.data.model.SourcesResponse
import com.aldisyarif.portalnews.paging.SourceNewsPagingSource
import com.aldisyarif.portalnews.repository.INewsRepository
import com.aldisyarif.portalnews.utils.Resources
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import retrofit2.HttpException
import java.io.IOException

class GetSourcesUseCase(
    private val repository: INewsRepository
): BaseUseCase() {

    operator fun invoke(query: String?, category: String?): Flow<PagingData<SourcesItem>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true
            ),
            pagingSourceFactory = {
                SourceNewsPagingSource(
                    query,
                    repository,
                    category,
                    apiKey
                )
            }
        ).flow
    }


//    private fun reposByTitle(query: String?): List<SourcesItem> {
//        re
//    }

}