package com.aldisyarif.portalnews.usecase

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.aldisyarif.portalnews.base.BaseUseCase
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.paging.NewsPagingSource
import com.aldisyarif.portalnews.repository.INewsRepository
import kotlinx.coroutines.flow.Flow

class GetArticlesUseCase(
    private val repository: INewsRepository
): BaseUseCase() {

    operator fun invoke(
        q: String,
        sources: String?,
    ): Flow<PagingData<ArticlesItem>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true
            ),
            pagingSourceFactory = {
                NewsPagingSource(
                    repository,
                    q,
                    null,
                    null,
                    sources,
                    apiKey
                )
            }
        ).flow
    }
}