package com.aldisyarif.portalnews.usecase

import com.aldisyarif.portalnews.base.BaseUseCase
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.repository.INewsRepository
import com.aldisyarif.portalnews.utils.Resources
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class GetListBookmarkNewsUseCase(
    private val repository: INewsRepository
): BaseUseCase() {
    operator fun invoke(): Flow<Resources<List<BookmarkEntity>>> {
        return flow {
            emit(Resources.loading())
            try {
                val db = repository.listBookmarkNews()
                db.collect{
                    emit(Resources.success(it))
                }
            } catch (e: Throwable){
                emit(Resources.error(e.localizedMessage ?: "Unknown Error", null))
            }
        }.flowOn(Dispatchers.IO)
    }
}