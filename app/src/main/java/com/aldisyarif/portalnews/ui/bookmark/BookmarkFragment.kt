package com.aldisyarif.portalnews.ui.bookmark

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.consts.Constant
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.databinding.FragmentBookmarkBinding
import com.aldisyarif.portalnews.utils.navigateSafe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookmarkFragment : Fragment() {

    private lateinit var adapter: BookmarkAdapter
    private val viewModel: BookmarkViewModel by viewModels()

    private lateinit var binding: FragmentBookmarkBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBookmarkBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initComponent()
    }

    private fun initComponent() {
        initViewModel()
        observeBookmark()
    }

    private fun initViewModel() {
        viewModel.getListBookmark()
    }

    private fun observeBookmark() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.listBookmarkSuccess.collect {
                if (!it.isNullOrEmpty()){
                    isBookmarkEmpty(false)
                    showListBookmark(it)
                } else {
                    isBookmarkEmpty(true)
                }
            }
        }

    }

    private fun isBookmarkEmpty(state: Boolean) {
        if(state){
            binding.imgBookmarkNotFound.visibility = View.VISIBLE
        } else {
            binding.imgBookmarkNotFound.visibility = View.GONE
        }
    }

    private fun showListBookmark(bookmarks: List<BookmarkEntity>) {
        adapter = BookmarkAdapter(bookmarks){
            val bundle = bundleOf(Constant.DATA_BOOKMARK to it)
            findNavController().navigateSafe(R.id.action_navigation_bookmark_to_detailFragment, bundle)
        }
        binding.apply {
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            recyclerView.adapter = adapter
        }
    }



}