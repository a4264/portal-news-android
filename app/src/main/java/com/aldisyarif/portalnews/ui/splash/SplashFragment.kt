package com.aldisyarif.portalnews.ui.splash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.databinding.FragmentSplashBinding
import com.aldisyarif.portalnews.utils.navigateSafe
import kotlinx.coroutines.delay

class SplashFragment : Fragment() {

    private lateinit var binding: FragmentSplashBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSplashBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initComponent()
    }

    private fun initComponent() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            delay(3000)
            findNavController().navigateSafe(R.id.action_splashFragment_to_navigation_dashboard)
        }
    }


}