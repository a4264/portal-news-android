package com.aldisyarif.portalnews.ui.bookmark

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.enums.RequestStatus
import com.aldisyarif.portalnews.usecase.GetListBookmarkNewsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookmarkViewModel @Inject constructor(
    private val getListBookmarkNewsUseCase: GetListBookmarkNewsUseCase
): ViewModel() {
    private val _listBookmarkSuccess = MutableStateFlow<List<BookmarkEntity>?>(null)
    val listBookmarkSuccess get() = _listBookmarkSuccess.asStateFlow()

    private val _loadingState = MutableStateFlow<Boolean?>(null)
    val loadingState get() = _loadingState.asStateFlow()

    fun getListBookmark(){
        viewModelScope.launch {
            getListBookmarkNewsUseCase().collect {
                when(it.requestStatus){
                    RequestStatus.LOADING -> {
                        _loadingState.emit(true)
                    }
                    RequestStatus.SUCCESS -> {
                        _loadingState.emit(false)
                        _listBookmarkSuccess.emit(it.data)
                    }
                    RequestStatus.ERROR -> {
                        _loadingState.emit(false)
                    }
                }
            }
        }
    }
}