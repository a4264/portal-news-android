package com.aldisyarif.portalnews.ui.category

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aldisyarif.portalnews.data.model.CategoryItem
import com.aldisyarif.portalnews.databinding.ItemCategoryBinding

class CategoryAdapter(
    private val context: Context,
    private val categories: List<CategoryItem>,
    private val onClick: (String) -> Unit
): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    inner class CategoryViewHolder(private val binding: ItemCategoryBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(category: CategoryItem) {
            binding.apply {
                icCategory.setImageResource(category.icon)
                tvTitleCategory.setText(category.title)

                btnToSource.setOnClickListener {
                    onClick.invoke(context.getString(category.title))
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val binding = ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.bind(category)
    }

    override fun getItemCount(): Int = categories.size
}