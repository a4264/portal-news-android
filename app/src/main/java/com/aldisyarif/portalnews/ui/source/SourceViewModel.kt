package com.aldisyarif.portalnews.ui.source

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.aldisyarif.portalnews.data.model.*
import com.aldisyarif.portalnews.usecase.GetSourcesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SourceViewModel @Inject constructor(
    private val getSourcesUseCase: GetSourcesUseCase,
    private val app: Application,
): AndroidViewModel(app) {

    val state : StateFlow<UiState>
    val accept: (UiAction) -> Unit
    val pagingDataFlow: Flow<PagingData<UiSourceModel>>

    private val category = MutableStateFlow("")
    private val _queryFlow = MutableStateFlow("")


    fun setCategory(ctgry: String){
        category.value = ctgry
    }

    init {
        val queryFlow = _queryFlow
        val lastQueryScrolled = MutableStateFlow(AppPreference.getPreference(app.applicationContext)
            .getString(AppPreference.LAST_QUERY_SCROLLED, ""))

        val actionStateFlow = MutableSharedFlow<UiAction>()
        val searces = actionStateFlow
            .filterIsInstance<UiAction.Search>()
            .distinctUntilChanged()
            .onStart { emit(UiAction.Search(query = queryFlow.value ?: "")) }
        val queryScrolled = actionStateFlow
            .filterIsInstance<UiAction.Scroll>()
            .shareIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000),
                replay = 1
            )
            .onStart { emit(UiAction.Scroll(currentQuery = lastQueryScrolled.value ?: "")) }


        pagingDataFlow = searces
            .flatMapLatest{ searcesSource(queryString = it.query) }
            .cachedIn(viewModelScope)

//        state = combine(
//            searces,
//            queryScrolled,
//            ::Pair
//        ).map { (search, scroll) ->
//            UiState(
//                query = search.query,
//                lastQueryScrolled = scroll.currentQuery,
//                // If the search query matches the scroll query, the user has scrolled
//                hasNotScrolledForCurrentSearch = search.query != scroll.currentQuery
//            )
//        }
//            .stateIn(
//                scope = viewModelScope,
//                started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000),
//                initialValue = UiState()
//            )

        state = searces
            .map { search ->
                UiState(
                    query = search.query,
                    lastQueryScrolled = "",
                    // If the search query matches the scroll query, the user has scrolled
                    hasNotScrolledForCurrentSearch = false
                )
            }
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000),
                initialValue = UiState()
            )

        accept = { action ->
            viewModelScope.launch {
                actionStateFlow.emit(action)
                Log.d("SourceViewModel", "accept ==  $action")
            }
        }
    }

    private fun searcesSource(queryString: String): Flow<PagingData<UiSourceModel>> =
        getSourcesUseCase(queryString, category = category.value)
            .map { pagingData ->
                pagingData.map { UiSourceModel.RepoItem(it) }
            }

}

object AppPreference {
    const val PREF_NAME = "search-Preference"
    const val LAST_QUERY_SCROLLED: String = "last_query_scrolled"
    const val LAST_SEARCH_QUERY: String = "last_search_query"

    fun setPreference(context: Context): SharedPreferences.Editor{
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
    }

    fun getPreference(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }
}