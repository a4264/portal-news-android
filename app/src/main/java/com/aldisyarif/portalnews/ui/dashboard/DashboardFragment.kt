package com.aldisyarif.portalnews.ui.dashboard

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.consts.Constant
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.databinding.FragmentDashboardBinding
import com.aldisyarif.portalnews.enums.RequestStatus
import com.aldisyarif.portalnews.utils.navigateSafe
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DashboardFragment : Fragment() {

    private lateinit var adapter: DashboardAdapter
    private val viewModel: DashboardVieModel by viewModels()

    private lateinit var binding: FragmentDashboardBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDashboardBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initComponent()
    }

    private fun initComponent() {
        initView()
        initViewModel()
        observeNews()
    }

    private fun initView() {
        showListTopHeadline()
    }

    private fun showListTopHeadline() {
        adapter = DashboardAdapter {
            val bundle = bundleOf(Constant.DATA_NEWS to it)
            findNavController().navigateSafe(R.id.action_navigation_dashboard_to_detailFragment, bundle)
        }
        binding.apply {
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            recyclerView.adapter = adapter
        }
        adapter.addLoadStateListener { loadState ->
            try {
                when(val currentState = loadState.source.refresh){
                    is LoadState.Loading -> {
                        isLoading(true)
                    }
                    is LoadState.NotLoading -> {
                        isLoading(false)
                    }
                    is LoadState.Error -> {
                        isLoading(false)
                        val extractException = currentState.error
                        Toast.makeText(requireContext(), extractException.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                }

            } catch (e: Exception){
                e.printStackTrace()
            }
        }

    }

    private fun initViewModel() {
        viewModel.getHeadLine()
    }

    private fun observeNews() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.listTopHeadlines.collect {
                when(it?.requestStatus){
                    RequestStatus.LOADING -> {
                        isLoading(true)
                    }
                    RequestStatus.SUCCESS -> {
                        it.data?.let { it1 ->
                            isLoading(false)
                            adapter.submitData(it1)
                        }
                    }
                    RequestStatus.ERROR -> {
                        isLoading(false)
                        Toast.makeText(requireContext(), "cek error", Toast.LENGTH_SHORT).show()
                    }
                    else -> Unit
                }
            }
        }
    }

    private fun isLoading(state: Boolean) {
        if (state){
            binding.apply {
                shimmerViewContainer.visibility = View.VISIBLE
                shimmerViewContainer.startShimmer()
            }
        } else {
            binding.apply {
                shimmerViewContainer.stopShimmer()
                shimmerViewContainer.visibility = View.GONE
            }
        }
    }


}