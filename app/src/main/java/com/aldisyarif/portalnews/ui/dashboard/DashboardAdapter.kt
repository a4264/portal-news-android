package com.aldisyarif.portalnews.ui.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.databinding.ItemHeadlineBinding
import com.aldisyarif.portalnews.paging.DiffUtilCallback
import com.bumptech.glide.Glide

class DashboardAdapter(
    private val onClick: (ArticlesItem) -> Unit
): PagingDataAdapter<ArticlesItem, DashboardAdapter.ViewHolder>(
    DiffUtilCallback.DIFF_CALLBACK) {

    inner class ViewHolder(private val binding: ItemHeadlineBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ArticlesItem) {
            binding.apply {
                Glide.with(binding.root)
                    .load(data.urlToImage)
                    .into(imgNews)
                tvTitleNews.text = data.title
                tvSource.text = data.source?.name
                tvUpdateAt.text = data.publishedAt

                itemView.setOnClickListener {
                    onClick.invoke(data)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = getItem(position = position)
        if (data != null){
            holder.bind(data)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemHeadlineBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

}