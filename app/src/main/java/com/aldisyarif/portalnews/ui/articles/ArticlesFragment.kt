package com.aldisyarif.portalnews.ui.articles

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.consts.Constant
import com.aldisyarif.portalnews.data.model.UiAction
import com.aldisyarif.portalnews.data.model.UiArticleModel
import com.aldisyarif.portalnews.data.model.UiState
import com.aldisyarif.portalnews.databinding.FragmentArticlesBinding
import com.aldisyarif.portalnews.utils.navigateSafe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ArticlesFragment : Fragment() {

    private lateinit var adapter: ArticleAdapter
    private var source: String? = null

    private val viewModel: ArticleViewModel by viewModels()

    private lateinit var binding: FragmentArticlesBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentArticlesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        source = arguments?.getString(Constant.SOURCE)

        initComponent()
    }

    private fun initComponent() {
        initToolbar()
        initViewModel()

        bindState(
            uiState = viewModel.state,
            pagingData = viewModel.pagingDataFlow,
            uiActions = viewModel.accept
        )
    }

    private fun bindState(
        uiState: StateFlow<UiState>,
        pagingData: Flow<PagingData<UiArticleModel>>,
        uiActions: (UiAction) -> Unit
    ) {
        adapter = ArticleAdapter {
            val bundle = bundleOf(Constant.DATA_NEWS to it)
            findNavController().navigateSafe(R.id.action_articlesFragment_to_detailFragment, bundle)
        }
        adapter.addLoadStateListener { loadState ->
            try {
                when(val currentState = loadState.source.refresh){
                    is LoadState.Loading -> {
                        isLoading(true)
                    }
                    is LoadState.NotLoading -> {
                        isLoading(false)
                    }
                    is LoadState.Error -> {
                        isLoading(false)
                        val extractException = currentState.error
                        Toast.makeText(requireContext(), extractException.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                }

            } catch (e: Exception){
                e.printStackTrace()
            }
        }

        binding.apply {
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            recyclerView.adapter = adapter
        }
        bindSearch(
            uiState = uiState,
            onQueryChanged = uiActions
        )

        bindList(
            repoAdapter = adapter,
            uiState = uiState,
            pagingData = pagingData,
            onScrollChanged = uiActions
        )
    }

    private fun bindList(
        repoAdapter: ArticleAdapter,
        uiState: StateFlow<UiState>,
        pagingData: Flow<PagingData<UiArticleModel>>,
        onScrollChanged: (UiAction) -> Unit
    ) {
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy != 0) onScrollChanged(UiAction.Scroll(currentQuery = uiState.value.query))
            }
        })

        lifecycleScope.launch {
            pagingData.collectLatest(repoAdapter::submitData)
        }
        lifecycleScope.launch {
            adapter.loadStateFlow.collect { loadState ->

                val isListEmpty = loadState.refresh is LoadState.NotLoading && repoAdapter.itemCount == 0
                showSearchEmpty(isListEmpty)
            }
        }
    }

    private fun showSearchEmpty(state: Boolean) {
        if (state){
            binding.notFound.visibility = View.VISIBLE
        } else {
            binding.notFound.visibility = View.GONE
        }
    }

    private fun bindSearch(uiState: StateFlow<UiState>, onQueryChanged: (UiAction) -> Unit) {
        binding.etSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH){
                val searchQuery = binding.etSearch.text.toString()
                updateSourceListFromInput(searchQuery, onQueryChanged)
                hideKeyword()
                true
            } else {
                false
            }
        }

        binding.etSearch.doOnTextChanged { text, start, before, count ->
            if (text.isNullOrEmpty()){
                updateSourceListFromInput("", onQueryChanged)
                binding.btDeleteText.visibility = View.GONE
            } else {
                binding.btDeleteText.visibility = View.VISIBLE
            }
        }

        binding.btDeleteText.setOnClickListener {
            updateSourceListFromInput("", onQueryChanged)
        }

        lifecycleScope.launch {
            uiState
                .map { it.query }
                .distinctUntilChanged()
                .collect(binding.etSearch::setText)
        }
    }

    private fun hideKeyword() {
        val view = activity?.currentFocus
        if (view != null) {
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun updateSourceListFromInput(query: String, onQueryChanged: (UiAction) -> Unit) {
        if (query.isNotEmpty()){
            binding.recyclerView.scrollToPosition(0)
            onQueryChanged(UiAction.Search(query = query))
        } else {
            onQueryChanged(UiAction.Search(query = ""))
        }
    }

    private fun initViewModel() {
        if(source != null) viewModel.setSource(source ?: "")
    }

    private fun initToolbar() {
        binding.btClose.setOnClickListener {
            findNavController().popBackStack()
        }

    }

    private fun isLoading(state: Boolean) {
        if (state){
            binding.apply {
                loadingState.visibility = View.VISIBLE
            }
        } else {
            binding.apply {
                loadingState.visibility = View.GONE
            }
        }
    }
}