package com.aldisyarif.portalnews.ui.articles

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.aldisyarif.portalnews.data.model.UiAction
import com.aldisyarif.portalnews.data.model.UiArticleModel
import com.aldisyarif.portalnews.data.model.UiState
import com.aldisyarif.portalnews.usecase.GetArticlesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticleViewModel @Inject constructor(
    private val getArticlesUseCase: GetArticlesUseCase
): ViewModel() {

    val state : StateFlow<UiState>
    val accept: (UiAction) -> Unit
    val pagingDataFlow: Flow<PagingData<UiArticleModel>>

    private val source = MutableStateFlow("")
    private val _queryFlow = MutableStateFlow("")

    fun setSource(src: String){
        source.value = src
    }

    init {
        val queryFlow = _queryFlow
        val actionStateFlow = MutableSharedFlow<UiAction>()
        val searces = actionStateFlow
            .filterIsInstance<UiAction.Search>()
            .distinctUntilChanged()
            .onStart { emit(UiAction.Search(query = queryFlow.value)) }

        pagingDataFlow = searces
            .flatMapLatest { searchArticle(queryString = it.query) }
            .cachedIn(viewModelScope)

        state = searces
            .map { search ->
                UiState(
                    query = search.query,
                    lastQueryScrolled = "",
                    hasNotScrolledForCurrentSearch = false
                )
            }
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000),
                initialValue = UiState()
            )

        accept = { action ->
            viewModelScope.launch {
                actionStateFlow.emit(action)
            }
        }
    }

    private fun searchArticle(queryString: String):  Flow<PagingData<UiArticleModel>> =
        getArticlesUseCase(queryString, source.value)
            .map { pagingData ->
                pagingData.map { UiArticleModel.RepoItem(it) }
            }
}