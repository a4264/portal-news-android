package com.aldisyarif.portalnews.ui.bookmark

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.databinding.ItemBookmarkBinding
import com.bumptech.glide.Glide

class BookmarkAdapter(
    private val list: List<BookmarkEntity>,
    private val onClick: (BookmarkEntity) -> Unit
): RecyclerView.Adapter<BookmarkAdapter.ViewHolder>()  {

    inner class ViewHolder(private val binding: ItemBookmarkBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(bookmark: BookmarkEntity) {
            binding.apply {
                Glide.with(binding.root)
                    .load(bookmark.urlToImage)
                    .into(imgNews)
                tvTitleNews.text = bookmark.title
                tvSource.text = bookmark.source?.name
                tvUpdateAt.text = bookmark.publishedAt

                itemView.setOnClickListener {
                    onClick.invoke(bookmark)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBookmarkBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bookmark = list[position]
        holder.bind(bookmark)
    }

    override fun getItemCount(): Int = list.size


}