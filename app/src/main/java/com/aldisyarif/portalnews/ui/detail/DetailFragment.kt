package com.aldisyarif.portalnews.ui.detail

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.consts.Constant
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.databinding.FragmentDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {

    private var dataDetail: BookmarkEntity? = null // from list
    private var dataBookmark: BookmarkEntity? = null

    private var isStateBookmark: Boolean = false

    private val viewModel: DetailViewModel by viewModels()
    private lateinit var binding: FragmentDetailBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initComponent()
    }

    private fun initComponent() {
        initToolbar()
        showDetailNews()
        initViewModel()
        observeDetail()
    }

    private fun initViewModel() {
        dataDetail?.let {
            viewModel.getDetailBookmark(it.title ?: "")
        }
    }

    private fun observeDetail() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.bookmarkSuccess.collect {
                if (it != null){
                    stateBookMark(true)
                }
            }
        }
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.unBookmarkSuccess.collect {
                if (it != null){
                    stateBookMark(false)
                }
            }
        }
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.detailNewsFromBookmark.collect {
                if (it != null){
                    dataBookmark = it
                    stateBookMark(true)
                } else {
                    stateBookMark(false)
                }
            }
        }
    }

    private fun initToolbar() {
        binding.apply {
            btClose.setOnClickListener {
                findNavController().popBackStack()
            }
            btnBookmark.setOnClickListener {
                if (isStateBookmark){
                    dataBookmark?.let {
                        viewModel.unBookmarkNews(it)
                    }
                } else {
                    dataDetail?.let {
                        viewModel.bookmarkNews(it)
                    }
                }
            }

        }
    }

    private fun showDetailNews() {
        val dataHeadline = arguments?.getParcelable(Constant.DATA_NEWS) as? ArticlesItem
        val bookmarkEntity = arguments?.getParcelable(Constant.DATA_BOOKMARK) as? BookmarkEntity

        // from list api
        if (dataHeadline != null){
            initWebViewDetail(dataHeadline.url ?: "")

            dataDetail = BookmarkEntity(
                publishedAt = dataHeadline.publishedAt,
                author = dataHeadline.author,
                urlToImage = dataHeadline.urlToImage,
                description = dataHeadline.description,
                source = dataHeadline.source,
                title = dataHeadline.title,
                url = dataHeadline.url,
                content = dataHeadline.content
            )
        }

        // from bookmark
        if(bookmarkEntity != null){
            initWebViewDetail(bookmarkEntity.url ?: "")
            dataDetail = bookmarkEntity
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebViewDetail(url: String) {
        binding.apply {
            webViewDetail.webViewClient = WebViewClient()
            webViewDetail.settings.domStorageEnabled = true
            webViewDetail.settings.loadsImagesAutomatically = true
            webViewDetail.settings.javaScriptEnabled = true
            webViewDetail.settings.useWideViewPort = true
            webViewDetail.settings.blockNetworkLoads = false
            webViewDetail.loadUrl(url)
        }
    }

    private fun stateBookMark(state: Boolean) {
        if (state){
            isStateBookmark = true
            binding.apply {
                btnBookmark.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.ic_bookmark))
            }
        } else {
            isStateBookmark = false
            binding.apply {
                btnBookmark.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.ic_unbookmark))
            }
        }
    }

}