package com.aldisyarif.portalnews.ui.source

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.consts.Constant
import com.aldisyarif.portalnews.data.model.UiAction
import com.aldisyarif.portalnews.data.model.UiSourceModel
import com.aldisyarif.portalnews.data.model.UiState
import com.aldisyarif.portalnews.databinding.FragmentSourceBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SourceFragment : Fragment() {
    private lateinit var adapter: SourceAdapter
    private var category: String? = null

    private val viewModel: SourceViewModel by viewModels()

    private lateinit var binding: FragmentSourceBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSourceBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        category = arguments?.getString(Constant.CATEGORY)

        initComponent()

    }

    private fun initComponent() {
        initToolbar()
//        showSourcesList()
        initViewModel()
//        observeSource()
        bindState(
            uiState = viewModel.state,
            pagingData = viewModel.pagingDataFlow,
            uiActions = viewModel.accept
        )
    }

    private fun bindState(
        uiState: StateFlow<UiState>,
        pagingData: Flow<PagingData<UiSourceModel>>,
        uiActions: (UiAction) -> Unit
    ) {
        adapter = SourceAdapter {
            val bundle = bundleOf(Constant.SOURCE to it.id)
            findNavController().navigate(R.id.action_sourceFragment_to_articlesFragment, bundle)
        }
        adapter.addLoadStateListener { loadState ->
            try {
                when(val currentState = loadState.source.refresh){
                    is LoadState.Loading -> {
                        isLoading(true)
                    }
                    is LoadState.NotLoading -> {
                        isLoading(false)
                    }
                    is LoadState.Error -> {
                        isLoading(false)
                        val extractException = currentState.error
                        Toast.makeText(requireContext(), extractException.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                }

            } catch (e: Exception){
                e.printStackTrace()
            }
        }

        binding.apply {
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            recyclerView.adapter = adapter
        }

        bindSearch(
            uiState = uiState,
            onQueryChanged = uiActions
        )
        bindList(
            repoAdapter = adapter,
            uiState = uiState,
            pagingData = pagingData,
            onScrollChanged = uiActions
        )
    }



    private fun bindSearch(
        uiState: StateFlow<UiState>,
        onQueryChanged: (UiAction) -> Unit
    ) {
        binding.etSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH){
                val searchQuery = binding.etSearch.text.toString()
                updateSourceListFromInput(searchQuery, onQueryChanged)
                hideKeyword()
                true
            } else {
                false
            }
        }

        binding.etSearch.doOnTextChanged { text, start, before, count ->
            if (text.isNullOrEmpty()){
                updateSourceListFromInput("", onQueryChanged)
                binding.btDeleteText.visibility = View.GONE
            } else {
                binding.btDeleteText.visibility = View.VISIBLE
            }
        }

        binding.btDeleteText.setOnClickListener {
            updateSourceListFromInput("", onQueryChanged)
        }

        lifecycleScope.launch {
            uiState
                .map { it.query }
                .distinctUntilChanged()
                .collect(binding.etSearch::setText)
        }
    }

    private fun hideKeyword() {
        val view = activity?.currentFocus
        if (view != null) {
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun updateSourceListFromInput(query: String, onQueryChanged: (UiAction) -> Unit) {
        if (query.isNotEmpty()){
            binding.recyclerView.scrollToPosition(0)
            onQueryChanged(UiAction.Search(query = query))
        } else {
            onQueryChanged(UiAction.Search(query = ""))
        }
    }

    private fun bindList(
        repoAdapter: SourceAdapter,
        uiState: StateFlow<UiState>,
        pagingData: Flow<PagingData<UiSourceModel>>,
        onScrollChanged: (UiAction) -> Unit
    ) {
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy != 0) onScrollChanged(UiAction.Scroll(currentQuery = uiState.value.query))
            }
        })

        lifecycleScope.launch {
            pagingData.collectLatest(repoAdapter::submitData)
        }

        lifecycleScope.launch {
            adapter.loadStateFlow.collect { loadState ->

                val isListEmpty = loadState.refresh is LoadState.NotLoading && repoAdapter.itemCount == 0
                showSearchEmpty(isListEmpty)
            }
        }

    }

    private fun showSearchEmpty(state: Boolean) {
        if (state){
            binding.notFound.visibility = View.VISIBLE
        } else {
            binding.notFound.visibility = View.GONE
        }
    }


//    private fun observeSource() {
//        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
//            viewModel.listSource.collect {
//
//                when(it?.requestStatus){
//                    RequestStatus.LOADING -> {
//                        loadingState(true)
//                    }
//                    RequestStatus.SUCCESS -> {
//                        it.data?.let { it1 ->
//                            Toast.makeText(requireContext(),    "cek", Toast.LENGTH_SHORT).show()
//                            loadingState(false)
//                            adapter.submitData(it1)
//                        }
//
//                    }
//                    RequestStatus.ERROR -> {
//                        loadingState(false)
//                        Toast.makeText(requireContext(), "cek error", Toast.LENGTH_SHORT).show()
//                    }
//                    else -> Unit
//                }
//            }
//        }
//
//    }

//    private fun loadingState(state: Boolean?) {
//        if (state == true){
//            binding.loadingState.visibility = View.VISIBLE
//        } else {
//            binding.loadingState.visibility = View.GONE
//        }
//    }

//    private fun showSourcesList() {
//        adapter = SourceAdapter()
//        binding.apply {
//            recyclerView.layoutManager = LinearLayoutManager(requireContext())
//            recyclerView.adapter = adapter
//        }
//    }

    private fun initViewModel() {
        if (category != null) viewModel.setCategory(category ?: "")
    }

    private fun initToolbar() {
        binding.btClose.setOnClickListener {
            findNavController().popBackStack()
        }
//        binding.etSearch.setOnEditorActionListener { _, actionId, _ ->
//            if (actionId == EditorInfo.IME_ACTION_SEARCH){
//                val searchQuery = binding.etSearch.text.toString()
//                if (searchQuery.isNotEmpty()){
//                    viewModel.onQueryChanged(searchQuery, category)
//                }
//            }
//            true
//        }
    }

    private fun isLoading(state: Boolean) {
        if (state){
            binding.apply {
                loadingState.visibility = View.VISIBLE
            }
        } else {
            binding.apply {
                loadingState.visibility = View.GONE
            }
        }
    }

}