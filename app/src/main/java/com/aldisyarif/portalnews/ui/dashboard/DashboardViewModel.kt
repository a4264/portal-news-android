package com.aldisyarif.portalnews.ui.dashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.usecase.GetTopHeadlineUseCase
import com.aldisyarif.portalnews.utils.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DashboardVieModel @Inject constructor(
    private val getTopHeadlineUseCase: GetTopHeadlineUseCase
): ViewModel() {

    private val _listTopHeadlines = MutableStateFlow<Resources<PagingData<ArticlesItem>>?>(null)
    val listTopHeadlines get() = _listTopHeadlines.asStateFlow()



    fun getHeadLine() {
        viewModelScope.launch {
            _listTopHeadlines.emit(Resources.loading())
            getTopHeadlineUseCase("us", null)
                .catch {
                    _listTopHeadlines.emit(Resources.error(it.localizedMessage ?: "Unknown Error", null))
                }
                .collectLatest {
                    _listTopHeadlines.emit(Resources.success(it))
                }
        }
    }

}