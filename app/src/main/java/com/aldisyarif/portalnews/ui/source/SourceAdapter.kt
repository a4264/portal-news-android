package com.aldisyarif.portalnews.ui.source

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aldisyarif.portalnews.data.model.SourcesItem
import com.aldisyarif.portalnews.data.model.UiSourceModel
import com.aldisyarif.portalnews.databinding.ItemSourceBinding
import com.aldisyarif.portalnews.paging.DiffUtilCallback

class SourceAdapter(
    private val onClick: (SourcesItem) -> Unit
): PagingDataAdapter<UiSourceModel, SourceAdapter.ViewHolder>(DiffUtilCallback.DIFF_CALLBACK_SOURCE) {


    inner class ViewHolder(private val binding: ItemSourceBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(source: SourcesItem) {
            Log.d("Adapter", " :::: $source")
            binding.apply {
                tvSourceName.text = source.name
                tvDescriptionSource.text = source.description

                itemView.setOnClickListener {
                    onClick.invoke(source)
                }

            }
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val uiModel = getItem(position = position)
        uiModel.let {
            if(uiModel is UiSourceModel.RepoItem){
                 holder.bind(uiModel.source)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemSourceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }


}