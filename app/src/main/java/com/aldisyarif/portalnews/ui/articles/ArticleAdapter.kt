package com.aldisyarif.portalnews.ui.articles

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.data.model.UiArticleModel
import com.aldisyarif.portalnews.databinding.ItemArticleBinding
import com.aldisyarif.portalnews.paging.DiffUtilCallback
import com.bumptech.glide.Glide

class ArticleAdapter(
    private val onClick: (ArticlesItem) -> Unit
): PagingDataAdapter<UiArticleModel, ArticleAdapter.ViewHolder>(DiffUtilCallback.DIFF_CALLBACK_ARTICLE) {

    inner class ViewHolder(private val binding: ItemArticleBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(article: ArticlesItem) {
            binding.apply {
                Glide.with(binding.root)
                    .load(article.urlToImage)
                    .into(imgNews)
                tvTitleNews.text = article.title
                tvSourceNews.text = article.source?.name
                tvUpdateAt.text = article.publishedAt

                itemView.setOnClickListener {
                    onClick.invoke(article)
                }
            }
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val uiModel = getItem(position = position)
        uiModel.let {
            if (uiModel is UiArticleModel.RepoItem){
                holder.bind(uiModel.article)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }
}