package com.aldisyarif.portalnews.ui.category

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.consts.Constant
import com.aldisyarif.portalnews.data.model.CategoryItem
import com.aldisyarif.portalnews.databinding.FragmentCategoryBinding
import com.aldisyarif.portalnews.utils.GenerateData
import com.aldisyarif.portalnews.utils.navigateSafe


class CategoryFragment : Fragment() {

    private lateinit var adapter: CategoryAdapter

    private lateinit var binding: FragmentCategoryBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCategoryBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initComponent()
    }

    private fun initComponent() {
        initView()
    }

    private fun initView() {
        showListCategory()
    }

    private fun showListCategory() {
        adapter = CategoryAdapter(requireContext(), GenerateData.getListCategory()) {
            val bundle = bundleOf(Constant.CATEGORY to it)
            findNavController().navigateSafe(R.id.action_navigation_category_to_sourceFragment, bundle)
        }
        binding.apply {
            recyclerView.layoutManager = GridLayoutManager(requireContext(), 3)
            recyclerView.adapter = adapter
        }
    }

}