package com.aldisyarif.portalnews.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aldisyarif.portalnews.data.local.entity.BookmarkEntity
import com.aldisyarif.portalnews.enums.RequestStatus
import com.aldisyarif.portalnews.usecase.DeleteUnBookmarkUseCase
import com.aldisyarif.portalnews.usecase.GetDetailBookmarkNewsUseCase
import com.aldisyarif.portalnews.usecase.SaveBookmarkNewsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val saveBookmarkNewsUseCase: SaveBookmarkNewsUseCase,
    private val deleteUnBookmarkUseCase: DeleteUnBookmarkUseCase,
    private val getDetailBookmarkNewsUseCase: GetDetailBookmarkNewsUseCase
): ViewModel() {
    private val _bookmarkSuccess = MutableStateFlow<Boolean?>(null)
    val bookmarkSuccess get() = _bookmarkSuccess.asStateFlow()

    private val _unBookmarkSuccess = MutableStateFlow<Boolean?>(null)
    val unBookmarkSuccess get() = _unBookmarkSuccess.asStateFlow()

    private val _detailNewsFromBookmark = MutableStateFlow<BookmarkEntity?>(null)
    val detailNewsFromBookmark get() = _detailNewsFromBookmark.asStateFlow()



    fun bookmarkNews(bookmarkEntity: BookmarkEntity){
        viewModelScope.launch {
            saveBookmarkNewsUseCase(bookmarkEntity).collect {
                when(it.requestStatus){
                    RequestStatus.LOADING -> {
                        _bookmarkSuccess.emit(false)
                    }
                    RequestStatus.SUCCESS -> {
                        if (it.data != null) _bookmarkSuccess.emit(true)
                    }
                    RequestStatus.ERROR -> {
                        _bookmarkSuccess.emit(true)
                    }
                }
            }
        }
    }

    fun unBookmarkNews(bookmarkEntity: BookmarkEntity){
        viewModelScope.launch {
            deleteUnBookmarkUseCase(bookmarkEntity).collect {
                when(it.requestStatus){
                    RequestStatus.LOADING -> {
                        _unBookmarkSuccess.emit(false)
                    }
                    RequestStatus.SUCCESS -> {
                        _unBookmarkSuccess.emit(true)
                    }
                    RequestStatus.ERROR -> {
                        _unBookmarkSuccess.emit(false)
                    }

                }
            }
        }
    }

    fun getDetailBookmark(title: String){
        viewModelScope.launch {
            getDetailBookmarkNewsUseCase(title).collect{
                when(it.requestStatus){
                    RequestStatus.LOADING -> {}
                    RequestStatus.SUCCESS -> {
                        _detailNewsFromBookmark.emit(it.data)
                    }
                    RequestStatus.ERROR -> {}
                }
            }
        }
    }

}