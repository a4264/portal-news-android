package com.aldisyarif.portalnews.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.aldisyarif.portalnews.R
import com.aldisyarif.portalnews.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        statusBarColor()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initComponent()
    }

    private fun statusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            window.statusBarColor = getColor(R.color.grey_80, )
        }
    }

    private fun initComponent() {
        bottomNav()
    }

    private fun bottomNav() {

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        val navController = navHostFragment.navController

        with(binding){
            btmNav.setupWithNavController(navController)
        }

        navController.addOnDestinationChangedListener(object : NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {
                if (destination.id == R.id.navigation_dashboard ||
                    destination.id == R.id.navigation_category ||
                    destination.id == R.id.navigation_bookmark
                ){
                    binding.btmNav.visibility = View.VISIBLE
                } else {
                    binding.btmNav.visibility = View.GONE
                }
            }

        })
    }

}