package com.aldisyarif.portalnews.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.repository.INewsRepository
import retrofit2.HttpException

class NewsPagingSource(
    private val repository: INewsRepository,
    private val q: String?,
    private val country: String?,
    private val category: String?,
    private val sources: String?,
    private val apiKey: String
): PagingSource<Int, ArticlesItem>() {

    override fun getRefreshKey(state: PagingState<Int, ArticlesItem>): Int? {
        return state.anchorPosition?.let {
            val anchorPage = state.closestPageToPosition(anchorPosition = it)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ArticlesItem> {
        return try {
            val position = params.key ?: INITIAL_PAGE_INDEX
            val response = repository.getTopHeadline(
                country,
                q,
                apiKey,
                category = category,
                sources = sources,
                page = position.toString(),
                pageSize = params.loadSize.toString()
            ).articles ?: listOf()

            LoadResult.Page(
                data = response,
                prevKey = if (position == INITIAL_PAGE_INDEX) null else position - 1,
                nextKey = if (response.isEmpty()) null else position + 1
            )
        } catch (e: Exception){
            return LoadResult.Error(e)
        } catch (e: HttpException){
            return LoadResult.Error(e)
        }
    }

    private companion object {
        const val INITIAL_PAGE_INDEX = 1
    }

}