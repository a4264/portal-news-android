package com.aldisyarif.portalnews.paging

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aldisyarif.portalnews.data.model.SourcesItem
import com.aldisyarif.portalnews.repository.INewsRepository
import retrofit2.HttpException

class SourceNewsPagingSource(
    private val query: String?,
    private val repository: INewsRepository,
    private val category: String?,
    private val apiKey: String
): PagingSource<Int, SourcesItem>() {

    override fun getRefreshKey(state: PagingState<Int, SourcesItem>): Int? {
        return state.anchorPosition?.let {
            val anchorPage = state.closestPageToPosition(anchorPosition = it)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, SourcesItem> {
        return try {
            val position = params.key ?: INITIAL_PAGE_INDEX

            val response = repository.getSources(
                apiKey,
                category = category,
                page = position.toString(),
                pageSize = params.loadSize.toString()
            ).sources?.filter {
                it.name?.lowercase()?.contains(query.toString().lowercase()) == true
            } ?: listOf()

            LoadResult.Page(
                data = response,
                prevKey = null,//if (position == INITIAL_PAGE_INDEX) null else position - 1,
                nextKey = null //if (response.isEmpty()) null else position + 1
            )

        } catch (e: Exception){
            return LoadResult.Error(e)
        } catch (e: HttpException){
            return LoadResult.Error(e)
        }
    }

    private companion object {
        const val INITIAL_PAGE_INDEX = 1
    }
}