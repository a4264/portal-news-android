package com.aldisyarif.portalnews.paging

import androidx.recyclerview.widget.DiffUtil
import com.aldisyarif.portalnews.data.model.ArticlesItem
import com.aldisyarif.portalnews.data.model.UiArticleModel
import com.aldisyarif.portalnews.data.model.UiSourceModel

object DiffUtilCallback {
    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ArticlesItem>(){
        override fun areItemsTheSame(oldItem: ArticlesItem, newItem: ArticlesItem): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ArticlesItem, newItem: ArticlesItem): Boolean {
            return oldItem.title == newItem.title
        }

    }

    val DIFF_CALLBACK_SOURCE = object : DiffUtil.ItemCallback<UiSourceModel>(){

        override fun areItemsTheSame(oldItem: UiSourceModel, newItem: UiSourceModel): Boolean {
            return (oldItem is UiSourceModel.RepoItem
                    && newItem is UiSourceModel.RepoItem
                    && oldItem.source.id == newItem.source.id
                    )
        }

        override fun areContentsTheSame(oldItem: UiSourceModel, newItem: UiSourceModel): Boolean {
            return oldItem == newItem
        }

    }

    val DIFF_CALLBACK_ARTICLE = object : DiffUtil.ItemCallback<UiArticleModel>(){
        override fun areItemsTheSame(oldItem: UiArticleModel, newItem: UiArticleModel): Boolean {
            return (oldItem is UiArticleModel.RepoItem
                    && newItem is UiArticleModel.RepoItem
                    && oldItem.article.url == newItem.article.url)
        }

        override fun areContentsTheSame(oldItem: UiArticleModel, newItem: UiArticleModel): Boolean {
            return oldItem == newItem
        }

    }
}