## Portal News
catalog news from https://newsapi.org/

## Screenshots

<img src="images/ss1.jpeg" width="280"> | <img src="images/ss2.jpeg" width="280"> | <img src="images/ss3.jpeg" width="280">
:-------------------------:|:-------------------------:|:-------------------------:
<em>Halaman Home</em>  |  <em>Halaman Category</em>  |  <em>Halaman Bookmark</em>


<img src="images/ss4.jpeg" width="280"> | <img src="images/ss5.jpeg" width="280"> | <img src="images/ss6.jpeg" width="280">
:-------------------------:|:-------------------------:|:-------------------------:
<em>Halaman Source</em>  |  <em>Halaman Articles</em>  |  <em>Halaman Detail</em>
